#!/bin/bash
dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
dnf install -y docker-ce
systemctl enable --now docker

curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh" | sudo bash
dnf install -y gitlab-runner
gitlab-runner register \
--non-interactive \
--executor "docker" \
--docker-image "docker:20.10.16" \
--docker-privileged \
--url https://gitlab.com \
--token "${TOKEN}" \
--description "docker-runner"

sed -i 's#volumes = \["/cache"\]#volumes = \["/cache", "/var/run/docker.sock:/var/run/docker.sock"\]#' /etc/gitlab-runner/config.toml
